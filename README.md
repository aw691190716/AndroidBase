#AndroidBase
Android快速开发基础库，涵盖Activity、Fragment基类，通用下拉刷新、加载更多界面、同意ViewHolder、Adapter、MD5，日志打印，Toast等

###AndroidStudio使用
```
compile 'com.haibin:androidbase:1.1.3'
```

###下拉刷新界面
```java
public class MainActivity extends BaseRecyclerViewActivity<String> {


    @Override
    public void onItemClick(int position, long itemId) {
        
    }

    @Override
    protected BaseRecyclerAdapter<String> getAdapter() {
        return new StringAdapter(this, BaseRecyclerAdapter.ONLY_FOOTER);
    }
    @Override
    public void onRefreshing() {
        //下拉刷新
    }

    @Override
    public void onLoadMore() {
        //加载更多
    }
}
```


###RecyclerAdapter
```java
public class StringAdapter extends BaseRecyclerAdapter<String> {
    public StringAdapter(Context context, int mode) {
        super(context, mode);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_list_string;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, String item, int position) {
        holder.bindText(R.id.title, item);
        holder.setImageResource(R.id.image, R.mipmap.ic_launcher);
    }
}
```
package com.haibin.androidbase.widget;

import android.animation.Animator;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Interpolator;

/**
 * Created by huanghaibin
 * on 16-5-15.
 */
public class FooterViewBehavior extends CoordinatorLayout.Behavior<View> {
    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();

    private int sinceDirectionChange;
    private AnimatorListener listener;

    public FooterViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        listener = new AnimatorListener();
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, View child, View directTargetChild, View target, int nestedScrollAxes) {
        return (nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0;
    }

    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, View child, View target, int dx, int dy, int[] consumed) {
        if (dy > 0 && sinceDirectionChange < 0 || dy < 0 && sinceDirectionChange > 0) {
            child.animate().cancel();
            sinceDirectionChange = 0;
        }
        sinceDirectionChange += dy;
        if (sinceDirectionChange > 10 && child.getVisibility() == View.VISIBLE) {
            hide(child);
        } else if (sinceDirectionChange < -10 && child.getVisibility() == View.GONE) {
            show(child);
        }
    }

    @Override
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, View child, View target) {
        super.onStopNestedScroll(coordinatorLayout, child, target);

    }

    private void hide(final View view) {
        ViewPropertyAnimator animator = view.animate().translationY(view.getHeight()).setInterpolator(INTERPOLATOR).setDuration(200);
        listener.setType(AnimatorListener.TYPE_HIDE);
        listener.setView(view);
        animator.setListener(listener);
        animator.start();
    }

    private void show(final View view) {
        ViewPropertyAnimator animator = view.animate().translationY(0).setInterpolator(INTERPOLATOR).setDuration(200);
        listener.setView(view);
        listener.setType(AnimatorListener.TYPE_SHOW);
        animator.setListener(listener);
        animator.start();
    }

    private class AnimatorListener implements Animator.AnimatorListener {
        private static final int TYPE_SHOW = 1;
        private static final int TYPE_HIDE = 2;
        View view;
        private int type;

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (type == TYPE_SHOW) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            if (type == TYPE_SHOW) {
                hide(view);
            } else {
                show(view);
            }
        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }

        public void setView(View view) {
            this.view = view;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}

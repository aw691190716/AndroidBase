package com.haibin.androidbase.widget.banner;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;


/**
 * Created by xtc on 2015/6/8
 */
public class ViewAutoSwitch extends ViewPager {

    private Handler handler;
    private int timeSpan = 5000;
    public final static int MSG = 1010;

    public void startAutoFlowTimer() {
        if (handler != null)
            return;
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                setCurrentItem(getCurrentItem() + 1);
                if (handler != null) {
                    Message message = handler.obtainMessage(MSG);
                    sendMessageDelayed(message, timeSpan);
                }
            }
        };
        Message message = handler.obtainMessage(MSG);
        handler.sendMessageDelayed(message, timeSpan);
    }

    public void stopAutoFlowTimer() {
        if (handler != null)
            handler.removeMessages(MSG);
        handler = null;
    }

    public ViewAutoSwitch(Context context) {
        super(context);
    }

    public ViewAutoSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //Log.v("bk", "ACTION_DOWN");
                stopAutoFlowTimer();
                break;
            case MotionEvent.ACTION_MOVE:
                // Log.v("bk", "ACTION_MOVE");
                stopAutoFlowTimer();
                break;
            case MotionEvent.ACTION_UP:
                //Log.v("bk", "ACTION_UP");
                startAutoFlowTimer();
                break;
            case MotionEvent.ACTION_CANCEL:
                //Log.v("bk", "ACTION_CANCEL");
                startAutoFlowTimer();
                break;
        }
        return super.onTouchEvent(ev);
    }
}

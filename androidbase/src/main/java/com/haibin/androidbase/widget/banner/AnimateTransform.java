package com.haibin.androidbase.widget.banner;

import android.view.View;

/**
 * Created by haibin
 * on 2016/11/25.
 */

public abstract class AnimateTransform {
    protected BannerAnimView mParent;

    public abstract void leftHideAnimation(View preView, float xStart, float xMove);

    public abstract void leftShowAnimation(View preView, float xStart, float xMove);

    public abstract void rightHideAnimation(View nextView, float xStart, float xMove);

    public abstract void rightShowAnimation(View nextView, float xStart, float xMove);

    public abstract void currentHideAnimation(View curView, float xStart, float xMove);

    public abstract void currentShowAnimation(View curView, float xStart, float xMove);

    public void setParent(BannerAnimView view) {
        this.mParent = view;
    }
}

package com.haibin.androidbase.widget.banner;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.support.v4.widget.ScrollerCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haibin
 * on 2016/11/18.
 */
@SuppressWarnings("all")
public class BannerAnimView extends FrameLayout implements View.OnClickListener {

    private PointInfo mStart, mEnd;
    private ViewAdapter mAdapter;
    private int mCurrentPosition;

    private float mX;
    private float mOffset;
    private float mAlpha;
    private float mNextAlpha;
    private boolean isUpdate;
    private boolean isClick;

    private VelocityTracker mTracker;
    private PagerObserver mObserver;

    private boolean isHideLeft, isHideRight;
    private boolean isShowLeft, isShowRight;
    ViewDragHelper mHelper;
    private AnimateTransform mTransform;
    ScrollerCompat mScroller;

    private View mPreView, mNextView;

    private OnItemClickListener mOnItemClickListener;
    private List<OnViewChangeListener> mViewChangeListeners;

    public BannerAnimView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mStart = new PointInfo();
        mEnd = new PointInfo();
        mTransform = new CardTransform();
        mTransform.setParent(this);
        mScroller = ScrollerCompat.create(context);
        setOnClickListener(this);
        mHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return false;
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                return left;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                return top;
            }
        });
    }

    public void setCurrentItem(int position) {
        int count = mAdapter.getCount();
        if (position <= count - 1 && count >= 2) {
            this.mCurrentPosition = position == 0 ? mCurrentPosition = count - 1 : position - 1;
            selected(count);
        }
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null && isClick) {
            mOnItemClickListener.onItemClick(mCurrentPosition);
        }
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return mHelper.shouldInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mAdapter == null) return super.onTouchEvent(event);
        int count = mAdapter.getCount();
        if (count <= 1) return super.onTouchEvent(event);

        int with = getWidth();
        int height = getHeight();

        mHelper.processTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mStart.setX(event.getX());
                mStart.setY(event.getY());
                mOffset = 0.0f;
                mAlpha = 0.0f;
                mTracker = VelocityTracker.obtain();
                requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_MOVE:
                mOffset = mStart.getX() - event.getX();
                mAlpha = (with - Math.abs(mOffset)) / with;
                mEnd.setX(event.getX());
                mEnd.setY(event.getY());
                mTracker.addMovement(event);
                mTracker.computeCurrentVelocity(1000);
                if (event.getY() >= height && mOffset == 0) {
                    isClick = true;
                    return false;
                }
                isClick = false;
                notifyViewStateChanged(OnViewChangeListener.STATE_DRAGGING);
                actionMove(count);
                break;
            case MotionEvent.ACTION_UP://结束时候
                isClick = (Math.abs(mOffset)) < 50;
                actionUp(count);
                notifyViewStateChanged(OnViewChangeListener.STATE_IDLE);
                break;
        }
        return super.onTouchEvent(event);
    }

    private void actionMove(int count) {
        View curView = getChildAt(0);//只维持一个View
        int w = getWidth();
        if (mPreView == null || mPreView.getParent() == null) {
            mPreView = mAdapter.instantiateItem(this, count - 1);
            addView(mPreView);
            mPreView.layout(-w, getTop(), 0, getBottom());
        }
        if (count == 2) {
            notifyViewScrolled(mCurrentPosition == 1 ? 0 : 1);
        } else {
            if (mOffset > 0) {//向左滑+1
                if (mNextView == null || mNextView.getParent() == null) {
                    mNextView = mAdapter.instantiateItem(this, mCurrentPosition + 1);
                    addView(mNextView);
                    mNextView.layout(w, getTop(), 2 * w, getBottom());
                }
                notifyViewScrolled(mCurrentPosition + 1);
                mScroller.startScroll(w - (int) mOffset, 0, (int) -mOffset, 0);
                postInvalidate();
            } else {
                notifyViewScrolled(mCurrentPosition - 1);
                int left = -getWidth();
                mScroller.startScroll(left, 0, left + (int) -mOffset, 0);
                postInvalidate();
            }
        }
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        int w = getWidth();

        if (!mScroller.computeScrollOffset()) {
            isHideRight = false;
        }

        if (mOffset > 0 && mNextView != null && mScroller.computeScrollOffset()) {//左滑+1
            if (!isHideRight) {
                int x = w - (int) mOffset;
                mNextView.layout(x, getTop(), x + w, getBottom());
                postInvalidate();
            } else {
                mNextView.layout(mScroller.getCurrX(), getTop(), mScroller.getCurrX() + w, getBottom());
            }


        } else {
            if (mNextView == null)
                return;
            if (mScroller.computeScrollOffset()) {//触发
                int x = -w + (int) Math.abs(mOffset);
                mPreView.layout(x, getTop(), x + w, getBottom());
                postInvalidate();
            } else {

            }
        }

    }

    private void actionUp(int count) {
        View curView = getChildAt(0);
        int w = getWidth();
        if (Math.abs(mOffset) >= getWidth() / 2) {//触发切换
            int index = 0;
            if (count == 2) {

            } else {
                if (mOffset <= 0) {//右滑-1

                } else {//左滑+1

                }
            }
            notifySelected();

        } else {
            if (mOffset == 0)
                return;
            if (mOffset <= 0) {//右滑-1

            } else {//左滑+1
                isHideRight = true;
                mScroller.startScroll(w, getTop(), 2 * w, getBottom(), 600);
            }
        }

    }

    private void selected(int count) {
        View curView = getChildAt(1);
        View preView = getChildAt(0);
        View nextView = getChildAt(2);
        int index = 0;
        if (count == 2) {

        } else {
            notifySelected();
        }
        notifyViewStateChanged(OnViewChangeListener.STATE_IDLE);
    }

    private void notifySelected() {
        if (mViewChangeListeners != null) {
            for (OnViewChangeListener listener : mViewChangeListeners) {
                listener.onViewSelected(mCurrentPosition);
            }
        }
    }

    private void notifyViewStateChanged(int state) {
        if (mViewChangeListeners != null) {
            for (OnViewChangeListener listener : mViewChangeListeners) {
                listener.onViewStateChanged(state);
            }
        }
    }

    private void notifyViewScrolled(int p) {
        if (mViewChangeListeners != null) {
            for (OnViewChangeListener listener : mViewChangeListeners) {
                listener.onViewScrolled(p, mOffset);
            }
        }
    }

    public void addOnViewChangeListener(OnViewChangeListener listener) {
        if (mViewChangeListeners == null) mViewChangeListeners = new ArrayList<>();
        mViewChangeListeners.add(listener);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }


    public void setAdapter(ViewAdapter adapter) {
        if (mAdapter != null) {
            mAdapter.setViewPagerObserver(null);
        }
        mAdapter = adapter;

        int count = mAdapter.getCount();

        if (count == 0)
            return;
        addView(mAdapter.instantiateItem(this, 0));
    }

    void dataSetChanged() {

    }

    private void hideAlphaAnimation(final View view, float curAlpha) {
        view.clearAnimation();
        view.setAlpha(curAlpha);

        view.animate().translationX(0).setDuration(500).setInterpolator(new DecelerateInterpolator()).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setAlpha(0.f);
            }
        });
    }

    private void showAlphaAnimation(final View view, float curAlpha) {
        view.clearAnimation();
        view.setAlpha(curAlpha);
        view.animate().alpha(1.f).setDuration(500).setInterpolator(new DecelerateInterpolator()).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setAlpha(1.f);
            }
        });

    }

    private static class PointInfo {
        private float x;
        private float y;

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }
    }

    public abstract static class ViewAdapter {
        private final DataSetObservable mObservable = new DataSetObservable();
        private DataSetObserver mViewPagerObserver;

        public abstract View instantiateItem(ViewGroup parent, int position);

        public abstract int getCount();

        public void destroyItem(ViewGroup parent, View view) {
            parent.removeView(view);
        }

        void setViewPagerObserver(DataSetObserver observer) {
            synchronized (this) {
                mViewPagerObserver = observer;
            }
        }

        public void notifyDataSetChange() {
            synchronized (this) {
                if (mViewPagerObserver != null) {
                    mViewPagerObserver.onChanged();
                }
            }
            mObservable.notifyChanged();
        }
    }

    public ViewAdapter getAdapter() {
        return mAdapter;
    }

    public interface OnViewChangeListener {
        static final int STATE_IDLE = -1;
        static final int STATE_DRAGGING = 1;

        void onViewScrolled(int position, float positionOffset);

        void onViewSelected(int position);

        void onViewStateChanged(int state);
    }

    private class PagerObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            dataSetChanged();
        }

        @Override
        public void onInvalidated() {
            dataSetChanged();
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}

package com.haibin.androidbase.widget.banner;

import android.view.View;

/**
 * Created by haibin
 * on 2016/11/25.
 */

public class CardTransform extends AnimateTransform {
    @Override
    public void leftHideAnimation(final View preView, float xStart, float xMove) {

    }

    @Override
    public void leftShowAnimation(final View preView, float xStart, float xMove) {

    }

    @Override
    public void rightHideAnimation(final View nextView, float xStart, float xMove) {

    }

    @Override
    public void rightShowAnimation(final View nextView, float xStart, float xMove) {

    }

    @Override
    public void currentHideAnimation(View curView, float xStart, float xMove) {

    }

    @Override
    public void currentShowAnimation(View curView, float xStart, float xMove) {

    }
}

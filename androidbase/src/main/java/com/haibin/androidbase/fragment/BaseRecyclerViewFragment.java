package com.haibin.androidbase.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.haibin.androidbase.R;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.widget.SuperRefreshLayout;

/**
 * Created by haibin
 * on 2016/10/18.
 */
@SuppressWarnings("unused")
public abstract class BaseRecyclerViewFragment<T> extends BaseFragment implements
        SuperRefreshLayout.SuperRefreshLayoutListener,
        BaseRecyclerAdapter.OnItemClickListener {
    protected RecyclerView mRecyclerView;
    protected SuperRefreshLayout mSuperRefreshLayout;
    protected BaseRecyclerAdapter<T> mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_base_revycler_view;
    }

    @Override
    protected void initView() {
        super.initView();
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recyclerView);
        mSuperRefreshLayout = (SuperRefreshLayout) mRootView.findViewById(R.id.refreshLayout);
        mSuperRefreshLayout.setSuperRefreshLayoutListener(this);
        mAdapter = getAdapter();
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setLayoutManager(getLayoutManager());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void initData() {
        mSuperRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSuperRefreshLayout.setRefreshing(true);
                onRefreshing();
            }
        });
    }

    protected abstract BaseRecyclerAdapter<T> getAdapter();

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }
}

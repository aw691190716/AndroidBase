package com.haibin.androidbase.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.haibin.androidbase.utils.T;

import butterknife.ButterKnife;

/**
 * Created by haibin
 * on 2016/10/18.
 */
@SuppressWarnings("unused")
public abstract class BaseFragment extends Fragment {
    protected View mRootView;
    protected LayoutInflater mInflater;
    protected int mThemeColor;
    protected Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView != null) {
            ViewGroup parent = (ViewGroup) mRootView.getParent();
            if (parent != null)
                parent.removeView(mRootView);
        } else {
            mRootView = inflater.inflate(getLayoutId(), container, false);
            mInflater = inflater;
            ButterKnife.bind(this, mRootView);
            initView();
            initTheme();
            initData();
        }
        return mRootView;
    }

    protected abstract
    @LayoutRes
    int getLayoutId();

    protected void initView() {

    }

    protected void initTheme() {
        mRootView.setBackgroundColor(mThemeColor);
    }

    protected void initData() {

    }

    @Override
    public void onDetach() {
        mContext = null;
        super.onDetach();
    }

    public String getResourceString(int strId) {
        return mContext != null ? mContext.getResources().getString(strId) : null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    protected void showToastShort(String message) {
        T.showToastShort(mContext, message);
    }

    protected void showToastShort(int strId) {
        T.showToastShort(mContext, strId);
    }

    protected void showToastLong(String message) {
        T.showToastLong(mContext, message);
    }

    protected void showToastLong(int strId) {
        T.showToastLong(mContext, strId);
    }
}

package com.haibin.androidbase.fragment;

import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;

import com.haibin.androidbase.R;

/**
 * Created by haibin
 * on 2016/10/18.
 */

public abstract class BaseToolBarFragment extends BaseFragment {
    protected Toolbar mToolBar;

    @Override
    protected void initView() {
        super.initView();
        mToolBar = (Toolbar) mRootView.findViewById(R.id.toolBar);
        if (mToolBar != null) {
            DrawableCompat.setTint(mToolBar.getNavigationIcon(), 0xFFFFFFFF);
            mToolBar.setTitleTextColor(0xFFFFFFFF);
        }
    }
}

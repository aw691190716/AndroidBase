package com.haibin.androidbase.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by haibin
 * on 2016/10/18.
 */
@SuppressWarnings("ALL")
public class ViewHolder extends RecyclerView.ViewHolder {
    private LayoutInflater mInflater;
    private Context mContext;

    public ViewHolder(LayoutInflater inflater, Context context, View itemView) {
        super(itemView);
        this.mInflater = inflater;
        this.mContext = context;
    }

    public ViewHolder(Context context, View itemView) {
        super(itemView);
        this.mContext = context;
    }

    public void bindText(int textViewId, CharSequence sequence) {
        TextView textView = getTextView(textViewId);
        textView.setText(sequence);
    }

    public void bindText(@IdRes int textViewId, @StringRes int strId) {
        bindText(textViewId, getString(strId));
    }

    public void setVisibility(@IdRes int viewId, int v) {
        getView(viewId).setVisibility(v);
    }

    public void setGone(@IdRes int viewId) {
        getView(viewId).setVisibility(View.GONE);
    }

    public void setVisible(@IdRes int viewId) {
        getView(viewId).setVisibility(View.VISIBLE);
    }

    public String getString(@StringRes int strId) {
        return mContext.getResources().getString(strId);
    }

    public void setImageResource(@IdRes int imageViewId, int drawableId) {
        ImageView imageView = (ImageView) itemView.findViewById(imageViewId);
        imageView.setImageResource(drawableId);
    }

    public void setBackgroundResource(@IdRes int imageViewId, int backgroundId) {
        ImageView imageView = getImageView(imageViewId);
        imageView.setBackgroundResource(backgroundId);
    }

    public ImageView getImageView(@IdRes int imageViewId) {
        return (ImageView) itemView.findViewById(imageViewId);
    }

    public View getView(@IdRes int viewId) {
        return itemView.findViewById(viewId);
    }

    public TextView getTextView(@IdRes int textViewId) {
        return (TextView) itemView.findViewById(textViewId);
    }

    public static ViewHolder create(Context context, ViewGroup parent, int layoutId) {
        return new ViewHolder(context, LayoutInflater.from(context).inflate(layoutId, parent, false));
    }
}

package com.haibin.androidbase.utils;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * 应用程序配置
 * Created by huanghaibin_dev
 * on 2016/4/12.
 */
@SuppressWarnings("unused")
public class DataConfig {

    private String mFileName;

    protected Context mContext;
    protected Properties mProperties;

    public DataConfig(Context context, String fileName) {
        this.mContext = context;
        this.mFileName = fileName;
        initProperties();
    }

    private void initProperties() {
        FileInputStream fis = null;
        mProperties = new Properties();
        try {
            File file = mContext.getDir(mFileName, Context.MODE_PRIVATE);
            fis = new FileInputStream(file.getPath() + File.separator
                    + mFileName);

            mProperties.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IO.close(fis);
        }
    }

    public Properties get() {
        FileInputStream fis = null;
        Properties props = new Properties();
        try {
            File dirConf = mContext.getDir(mFileName, Context.MODE_PRIVATE);
            fis = new FileInputStream(dirConf.getPath() + File.separator
                    + mFileName);

            props.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IO.close(fis);
        }
        return props;
    }

    private void saveProperties(Properties p) {
        FileOutputStream fos = null;
        try {
            File dirConf = mContext.getDir(mFileName, Context.MODE_PRIVATE);
            File conf = new File(dirConf, mFileName);
            fos = new FileOutputStream(conf);

            p.store(fos, null);
            fos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IO.close(fos);
        }
    }

    /**
     * 新增配置
     *
     * @param ps
     */
    public void putProperties(Properties ps) {
        Properties properties = get();
        mProperties.putAll(ps);
        saveProperties(mProperties);
    }

    public void clearProperties(String... key) {
        Properties properties = get();
        for (String k : key)
            mProperties.remove(k);
        saveProperties(mProperties);
    }

    /**
     * 获取配置
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public String getString(String key, String defaultValue) {
        Properties properties = get();
        return (mProperties != null) ? mProperties.getProperty(key, defaultValue) : null;
    }

    public int getInt(String key, int defaultValue) {
        Properties properties = get();
        return (mProperties != null) ? Integer.parseInt(mProperties.getProperty(key, String.valueOf(defaultValue))) : -1;
    }

    public void put(String key, String value) {
        Properties properties = get();
        mProperties.setProperty(key, value);
        saveProperties(mProperties);
    }

    public void put(String key, int value) {
        Properties properties = get();
        mProperties.put(key, String.valueOf(value));
        saveProperties(mProperties);
    }
}

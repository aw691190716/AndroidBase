package com.haibin.androidbase.utils;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by haibin
 * on 2016/10/18.
 */
@SuppressWarnings("all")
public final class SerializableCacheManager {

    /**
     * 保存对象
     *
     * @param ser  ser
     * @param file file
     */
    public static boolean saveObject(Context context, Serializable ser,
                                     String file) {
        File f = new File(file);
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = context.openFileOutput(file, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ser);
            oos.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            IO.close(fos, oos);
        }
    }


    /**
     * 读取对象
     *
     * @param context context
     * @param file    file
     * @return 读取对象
     */
    public static Serializable readObject(Context context, String file) {
        if (!isExistDataCache(context, file))
            return null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = context.openFileInput(file);
            ois = new ObjectInputStream(fis);
            return (Serializable) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof InvalidClassException) {
                File data = context.getFileStreamPath(file);
                if (data.exists()) {
                    data.delete();
                }
            }
        } finally {
            IO.close(fis, ois);
        }
        return null;
    }

    /**
     * 判断缓存是否存在
     *
     * @param cacheFile cacheFile
     * @return exist
     */
    public static boolean isExistDataCache(Context context, String cacheFile) {
        if (context == null)
            return false;
        boolean exist = false;
        File data = context.getFileStreamPath(cacheFile);
        if (data.exists())
            exist = true;
        return exist;
    }
}

package com.haibin.androidbase.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;



/**
 * Created by haibin
 * on 2016/10/18.
 */
@SuppressWarnings("all")
public class T {

    private static Toast mToast;

    private static void makeText(Context context, CharSequence text, int gravity, int duration) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, duration);
        } else {
            mToast.setDuration(duration);
            mToast.setText(text);
        }
        if (gravity != 0) {
            mToast.setGravity(gravity, 0, 0);
        } else {
            mToast.setGravity(Gravity.BOTTOM, 0, 200);
        }
        mToast.show();
    }

    public static void showToastLong(Context context, int res) {
        showToastLong(context, context.getText(res));
    }

    public static void showToastLong(Context context, CharSequence message) {
        makeText(context, message, 0, Toast.LENGTH_LONG);
    }


    public static void showToastShort(Context context, int res) {
        showToastShort(context, context.getText(res));
    }

    public static void showToastShort(Context context, CharSequence message) {
        makeText(context, message, 0, Toast.LENGTH_SHORT);
    }

    public static void showToastInGravity(Context context, int gravity, int res) {
        showToastInGravity(context, gravity, context.getText(res));
    }

    public static void showToastInGravity(Context context, int gravity,
                                          CharSequence message) {
        makeText(context, message, gravity, Toast.LENGTH_SHORT);
    }

    public static void showToastWithIcon(Context context, int drawable, int res) {
        showToastWithIcon(context, drawable, context.getText(res));
    }

    public static void showToastWithIcon(Context context, int drawable,
                                         CharSequence message) {
        Toast toast = getToast(context, drawable, message);
        toast.show();
    }

    public static void showToastWithIconInGravity(Context context,
                                                  int drawable, int gravity, int res) {
        showToastWithIconInGravity(context, drawable, gravity,
                context.getText(res));
    }

    public static void showToastWithIconInGravity(Context context,
                                                  int drawable, int gravity, CharSequence message) {
        Toast toast = getToast(context, drawable, message);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }

    private static Toast getToast(Context context, int drawable,
                                  CharSequence message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        LinearLayout linearLayout = (LinearLayout) toast.getView();
        ImageView iv = new ImageView(context);
        iv.setImageResource(drawable);
        linearLayout.addView(iv, 0);
        return toast;
    }

    public static void showCustomToast(Context context, int layoutId, int drawable,
                                       CharSequence message, int gravity) {
        Toast toast = new Toast(context);
        LinearLayout view = (LinearLayout) LayoutInflater.from(context)
                .inflate(layoutId, null);
        if (drawable != 0) {
            ImageView iv = (ImageView) view.getChildAt(0);
            iv.setImageResource(drawable);
        }
        if (message != null) {
            TextView tv = (TextView) view.getChildAt(1);
            tv.setText(message);
        }
        if (gravity != 0) {
            toast.setGravity(gravity, 0, 0);
        }
        toast.setView(view);
        toast.show();
    }
}

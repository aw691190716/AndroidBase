package com.haibin.androidbase.utils;

import java.io.Closeable;

/**
 * Created by haibin
 * on 2016/10/18.
 */

public class IO {
    public static void close(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

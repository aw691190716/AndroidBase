package com.haibin.androidbase.activity;

import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;

/**
 * Created by haibin
 * on 2016/10/19.
 */

public abstract class BaseBackActivity extends BaseToolBarActivity {
    @Override
    protected void initView() {
        super.initView();
        if (mToolBar != null) {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(false);
            }
            DrawableCompat.setTint(mToolBar.getNavigationIcon(), 0xFFFFFFFF);
            mToolBar.setTitleTextColor(0xFFFFFFFF);
        }
    }
}

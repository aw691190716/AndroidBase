package com.haibin.androidbase.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.haibin.androidbase.activity.swipe.SwipeBackActivity;
import com.haibin.androidbase.utils.T;

import java.io.Serializable;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by haibin
 * on 2016/10/18.
 */
@SuppressWarnings("all")
public abstract class BaseActivity extends SwipeBackActivity {
    protected View mRootView;
    protected int mThemeColor;
    private Fragment mFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRootView = LayoutInflater.from(this).inflate(getLayoutId(), null);
        setContentView(mRootView);
        ButterKnife.bind(this);
        initView();
        initTheme();
        initData();
    }

    protected abstract
    @LayoutRes
    int getLayoutId();

    protected void initView() {

    }

    protected void initTheme() {

    }

    protected void initData() {

    }

    public void addFragment(int frameLayoutId, Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (fragment.isAdded()) {
                if (mFragment != null) {
                    transaction.hide(mFragment).show(fragment);
                } else {
                    transaction.show(mFragment);
                }
            } else {
                if (mFragment != null) {
                    transaction.hide(mFragment).add(frameLayoutId, fragment);
                } else {
                    transaction.add(frameLayoutId, fragment);
                }
            }
            mFragment = fragment;
            transaction.commit();
        }
    }

    /**
     * 是否在前台
     *
     * @return isOnForeground
     */
    public boolean isOnForeground() {
        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();

        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }

    public void startOrCloseKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void closeKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public void openKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public String getResourceString(int strId) {
        return getResources().getString(strId);
    }

    public Serializable getSerializable(String key) {
        Bundle bundle = getBundle();
        return bundle == null ? null : bundle.getSerializable(key);
    }

    public Bundle getBundle() {
        return getIntent().getExtras();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    protected void showToastShort(String message) {
        T.showToastShort(this, message);
    }

    protected void showToastShort(int strId) {
        T.showToastShort(this, strId);
    }

    protected void showToastLong(String message) {
        T.showToastLong(this, message);
    }

    protected void showToastLong(int strId) {
        T.showToastLong(this, strId);
    }
}

package com.haibin.androidbase.activity;

import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.haibin.androidbase.R;

/**
 * Created by haibin
 * on 2016/10/18.
 */

public abstract class BaseToolBarActivity extends BaseActivity {
    protected Toolbar mToolBar;

    @Override
    protected void initView() {
        mToolBar = (Toolbar) findViewById(R.id.toolBar);
        if (mToolBar != null) {
            setSupportActionBar(mToolBar);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}

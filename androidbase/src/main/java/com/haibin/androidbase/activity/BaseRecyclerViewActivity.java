package com.haibin.androidbase.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.haibin.androidbase.R;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.widget.SuperRefreshLayout;

/**
 * Created by haibin
 * on 2016/10/18.
 */

public abstract class BaseRecyclerViewActivity<T> extends BaseBackActivity implements
        SuperRefreshLayout.SuperRefreshLayoutListener, BaseRecyclerAdapter.OnItemClickListener {
    protected RecyclerView mRecyclerView;
    protected SuperRefreshLayout mSuperRefreshLayout;
    protected BaseRecyclerAdapter<T> mAdapter;
    protected boolean mIsRefresh;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_base_recycler_view;
    }

    @Override
    protected void initView() {
        super.initView();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mSuperRefreshLayout = (SuperRefreshLayout) findViewById(R.id.refreshLayout);
        mSuperRefreshLayout.setSuperRefreshLayoutListener(this);
        mAdapter = getAdapter();
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setLayoutManager(getLayoutManager());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void initData() {
        mSuperRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSuperRefreshLayout.setRefreshing(true);
                onRefreshing();
            }
        });
    }

    @Override
    public void onItemClick(int position, long itemId) {

    }

    @Override
    public void onRefreshing() {
        mIsRefresh = true;
    }

    protected abstract BaseRecyclerAdapter<T> getAdapter();

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }
}

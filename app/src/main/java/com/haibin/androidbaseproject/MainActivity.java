package com.haibin.androidbaseproject;


import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.haibin.androidbase.activity.BaseRecyclerViewActivity;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.widget.banner.BannerAnimView;
import com.haibin.androidbase.widget.banner.BannerView;
import com.haibin.androidbase.widget.banner.CircleBannerIndicator;

public class MainActivity extends BaseRecyclerViewActivity<String> implements Runnable {

    BannerAnimView.ViewAdapter adapter;
    BannerAnimView mBanner;
    Handler handler = new Handler();
    boolean isScroll;
    private int p = 0;

    @Override
    protected void initView() {
        super.initView();
        setSwipeBackEnable(false);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_banner_view, null);
        mBanner = (BannerAnimView) view.findViewById(R.id.banner);
        CircleBannerIndicator indicator = (CircleBannerIndicator) view.findViewById(R.id.indicator);
        adapter = new BannerAnimView.ViewAdapter() {
            @Override
            public View instantiateItem(ViewGroup parent, final int position) {
                ImageView view = new ImageView(MainActivity.this);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                view.setLayoutParams(params);
                if (position == 0) {
                    view.setImageResource(R.drawable.a);
                } else if (position == 1) {
                    view.setImageResource(R.drawable.b);
                } else if (position == 2) {
                    view.setImageResource(R.drawable.c);
                } else if (position == 3) {
                    view.setImageResource(R.drawable.d);
                } else if (position == 4) {
                    view.setImageResource(R.drawable.e);
                }
                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showToastShort(" --  " + position);
                    }
                });
                parent.addView(view);
                return view;
            }

            @Override
            public int getCount() {
                return 5;
            }
        };
        mBanner.setAdapter(adapter);
        mBanner.setOnItemClickListener(new BannerAnimView.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                showToastShort((position) + "个");
            }
        });
        mBanner.addOnViewChangeListener(new BannerAnimView.OnViewChangeListener() {
            @Override
            public void onViewScrolled(int position, float positionOffset) {
                isScroll = true;
                handler.removeCallbacks(MainActivity.this);
            }

            @Override
            public void onViewSelected(int position) {
                p = position;
                isScroll = false;
            }

            @Override
            public void onViewStateChanged(int state) {
                isScroll = state != BannerView.OnViewChangeListener.STATE_IDLE;
            }
        });

        handler.postDelayed(this, 5000);
        mAdapter.setHeaderView(view);
    }

    @Override
    public void run() {
//        if (!isScroll) {
//            p = (p + 1) % adapter.getCount();
//            mBanner.setCurrentItem(p);
//        }
//        handler.removeCallbacks(this);
//        handler.postDelayed(this, 5000);
    }

    @Override
    protected void initData() {
        super.initData();
    }

    @Override
    public void onItemClick(int position, long itemId) {

    }

    @Override
    protected BaseRecyclerAdapter<String> getAdapter() {
        return new StringAdapter(this, BaseRecyclerAdapter.BOTH_HEADER_FOOTER);
    }


    @Override
    public void onRefreshing() {
        mSuperRefreshLayout.setRefreshing(false);
        for (int i = 0; i < 20; i++) {
            mAdapter.addItem("第" + i + "条数据");
        }
    }

    @Override
    public void onLoadMore() {

    }
}

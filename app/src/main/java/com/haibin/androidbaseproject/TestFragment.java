package com.haibin.androidbaseproject;

import android.os.Bundle;
import android.widget.TextView;

import com.haibin.androidbase.fragment.BaseFragment;

/**
 * Created by haibin on 2016/11/1.
 */

public class TestFragment extends BaseFragment {
    TextView text;

    public static TestFragment newInstance(int index) {
        TestFragment fragment = new TestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("index", index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_test;
    }

    @Override
    protected void initView() {
        super.initView();
        text = (TextView) mRootView.findViewById(R.id.text);
        text.setText(getArguments().getInt("index") + "");
    }
}

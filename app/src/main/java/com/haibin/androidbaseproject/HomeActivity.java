package com.haibin.androidbaseproject;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.haibin.androidbase.activity.BaseBackActivity;
import com.haibin.androidbase.widget.banner.BannerAnimView;
import com.haibin.androidbase.widget.banner.CircleBannerIndicator;

import butterknife.Bind;

/**
 * Created by haibin
 * on 2016/10/19.
 */

public class HomeActivity extends BaseBackActivity {

    @Bind(R.id.banner)
    BannerAnimView mBanner;

    @Bind(R.id.indicator)
    CircleBannerIndicator indicator;

    BannerAnimView.ViewAdapter mAdapter;

    Handler handler = new Handler();
    private int p = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.layout_banner_view;
    }

    @Override
    protected void initView() {
        super.initView();
        setSwipeBackEnable(false);
        mAdapter = new BannerAnimView.ViewAdapter() {
            @Override
            public View instantiateItem(ViewGroup parent, int position) {
                ImageView view = new ImageView(HomeActivity.this);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                view.setLayoutParams(params);

                if (position == 0) {
                    view.setImageResource(R.drawable.a);
                } else if (position == 1) {
                    view.setImageResource(R.drawable.b);
                } else {
                    view.setImageResource(R.drawable.c);
                }
                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                //parent.addView(view);
                return view;
            }

            @Override
            public int getCount() {
                return 3;
            }
        };
        mBanner.setAdapter(mAdapter);
        mBanner.setOnItemClickListener(new BannerAnimView.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }
        });
        mBanner.addOnViewChangeListener(new BannerAnimView.OnViewChangeListener() {
            @Override
            public void onViewScrolled(int position, float positionOffset) {

            }

            @Override
            public void onViewSelected(int position) {

            }

            @Override
            public void onViewStateChanged(int state) {

            }
        });

        //indicator.bindBannerView(mBanner);
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                p = (p + 1) % mAdapter.getCount();
//                mBanner.setCurrentItem(p);
//
//                handler.postDelayed(this, 5000);
//            }
//        }, 5000);
    }
}

package com.haibin.androidbaseproject;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.haibin.androidbaseproject.R;
import com.haibin.androidbase.activity.BaseActivity;
import com.haibin.androidbase.widget.banner.ViewAutoSwitch;

/**
 * Created by haibin
 * on 2016/11/28.
 */

public class BannerActivity extends BaseActivity {

    ViewAutoSwitch mPager;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_b;
    }

    @Override
    protected void initView() {
        super.initView();
        mPager = (ViewAutoSwitch) findViewById(R.id.banner1);
        mPager.setPageTransformer(true,new ZoomOutPageTransformer());


        mPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return Integer.MAX_VALUE;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                ImageView view = new ImageView(BannerActivity.this);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                view.setLayoutParams(params);

                if (position == 0) {
                    view.setImageResource(com.haibin.androidbaseproject.R.drawable.a);
                } else if (position == 1) {
                    view.setImageResource(com.haibin.androidbaseproject.R.drawable.b);
                } else {
                    view.setImageResource(com.haibin.androidbaseproject.R.drawable.c);
                }
                container.addView(view);
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((ImageView)object);
            }
        });
        mPager.setCurrentItem(3000);
        mPager.startAutoFlowTimer();
    }
}

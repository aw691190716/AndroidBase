package com.haibin.androidbaseproject;

import android.content.Context;
import android.view.ViewGroup;

import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;

/**
 * Created by haibin
 * on 2016/10/18.
 */

public class StringAdapter extends BaseRecyclerAdapter<String>  {
    public StringAdapter(Context context, int mode) {
        super(context, mode);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_string;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, String item, int position) {
        holder.bindText(R.id.title, item);
        holder.setImageResource(R.id.image, R.mipmap.ic_launcher);
    }
}

package com.haibin.androidbaseproject;

import com.haibin.androidbase.activity.BaseBackActivity;

/**
 * Created by haibin
 * on 2016/11/25.
 */

public class DragAvtivity extends BaseBackActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_drag;
    }

    @Override
    protected void initView() {
        super.initView();
        setSwipeBackEnable(false);
    }
}
